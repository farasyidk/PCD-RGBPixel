var image = null;
var gambar = null;
function upload() {
  var fileinput = gambar = document.getElementById("finput");
  image = new SimpleImage(fileinput);
  var canvas = document.getElementById("can");
  
  image.drawTo(canvas);
}

function reset() {
    image = image = new SimpleImage(gambar);
    var canvas = document.getElementById("can");
    image.drawTo(canvas);
}

function convert(proses) {
  
    for (var pixel of image.values()) {
    var avg = (pixel.getRed()+pixel.getGreen()+pixel.getBlue())/3;
    if (proses == "biner") {
        if (avg < 128) {
            avg = 0;
        } else {
            avg = 255;
        }
    } else if(proses == 'brightness') {
        var brigh = parseInt(document.getElementById("myRange").value);
        if (brigh > 50) {
            brigh = brigh - 50;
            avg = avg + brigh;
            if (avg > 255) { avg = 255; }
        } else {
            avg = avg - brigh;
            if (avg <0) { avg = 0;}
        }
    }  else if (proses == 'bit') {
        avg = 64 * (avg/64);
    } else if (proses == 'negatif') {
        avg = 255 - avg;    
    } 

    if (proses == 'contrast') {
        var red,green,blue;
        var tc = 2,
            g = 50;
        red = tc * (pixel.getRed() - g) + g;
        green = tc * (pixel.getGreen() - g) + g;
        blue = tc * (pixel.getBlue() - g) + g;
        if (red < 0) { red = 0;} else if (red > 255) { red = 255;}
        if (green < 0) {
            green = 0;
        }
        else if(green > 255) {
            green = 255;
        }
        if (blue < 0) {
            reblued = 0;
        }
        else if(blue > 255) {
            blue = 255;
        }
        
        pixel.setRed(red);
        pixel.setGreen(green);
        pixel.setBlue(blue);
    } else {
        pixel.setRed(avg);
        pixel.setGreen(avg);
        pixel.setBlue(avg);
    }
    
  }
  //display new image
  var canvas = document.getElementById("can");
  image.drawTo(canvas);
  //alert(image.values());
}

var button = document.getElementById('btn-download');
button.addEventListener('click', function (e) {
    canvas = canvas = document.getElementById("can");
    var dataURL = canvas.toDataURL('image/png');
    button.href = dataURL;
});